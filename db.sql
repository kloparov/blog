create database blog;
use blog;

create table t_article(
	id int not null auto_increment primary key,
	name varchar(100) not null,
	content varchar(2000) not null
);


create table t_user(
	username varchar(50) not null primary key,
	password varchar(10) not null
);

insert into t_user values ('koki','@admin#123');
