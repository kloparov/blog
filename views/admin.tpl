<html>
	<head>
		<link href="static/css/admin-style.css" rel="stylesheet">
		<link href="static/css/buttons.css" rel="stylesheet">
		<link href="static/css/jNotify.jquery.css" rel="stylesheet">

		<script src="static/js/jquery-1.10.2.js"></script>
		<script src="static/js/jNotify.jquery.js"></script>
		<script src="static/js/admin.js"></script>

		<script type="text/javascript">
			
		</script>
	</head>
	<body>
		<div id="article-list" class="position">
			<div id="list-header" class="header shadow"> Posts:</div>
			<ul class="border shadow">
				%for article in postlist:
				    <li> {{ article["name"] }}
				%end
			</ul>
		</div>

		<div id="article" class="position ">
			<div id="article-header" class="header shadow"> Article`s content:</div>
			<div id="article-content" class="border shadow"> 
				<input type="text" id="name" name="article-name" maxlength="100" size="100" class="align" placeholder="name..">
				<textarea id="content" name="article-content" class="align" placeholder="content..."></textarea>

				<button id="save" class="button">Update</button>
				<button id="delete" class="button">Delete</button>
				<button id="new" class="button">Save new</button>
				<button id="clear" class="button">Clear</button>
			</div> 
			
		</div>
	</body>
</html>