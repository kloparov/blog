<html>
	<head>
		<meta name="description" content="Just another dev blog about programming with Java, Python, JavaScript.">
		<meta name="keywords" content="Java, JS, JavaScript, CSS">
		<meta charset="UTF-8">

		<title>47lines.com - Just another dev blog about Java, Python, JS...</title>
		<link href="static/css/style.css" rel="stylesheet">
		<link href="static/css/prettify.css" rel="stylesheet">
		
		<script src="static/js/jquery-1.10.2.js"></script>
		<script src="static/js/run_prettify.js"></script>
		<script src="static/js/prettify.js"></script>
		<script src="static/js/nano.js"></script>
		
		<script src="static/js/script.js"></script>
	</head>
	<body>
		<div id="line"></div>
		<div style="float:right;"> 
			<div id="brand-description">Just another dev blog about Java, Python, JS...</div>
		</div>
		<div id="header">
			<div id="brand">
				<div id="title">Posts: </div>	
			</div>
			<ul id="list">
				% i = 0
				%for article in postlist:
				    <li> {{ article["name"] }}
				%end
			</ul>
			<div id="article"></div>
		</div>
		<div id="footer">
			<div id="copyright">Copyright © 2013 </div>
		</div>
	</body>
</html>

<script type="text/html" id="template">
	<div id="article-title"> {article.name} </div>
	<div id="article-content"> {article.content} </div>
	<div id="disqus_panel"> 
		<div id="disqus_thread"></div>
	    <script type="text/javascript">
	        /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
	        var disqus_shortname = '47lines'; // required: replace example with your forum shortname

	        /* * * DON'T EDIT BELOW THIS LINE * * */
	        (function() {
	            var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
	            dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
	            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
	        })();
	    </script>
	    <noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
	    <a href="http://disqus.com" class="dsq-brlink">comments powered by <span class="logo-disqus">Disqus</span></a>
	</div>
</script>