<html>
	<head>
		<link href="static/css/login.css" rel="stylesheet">

		<script src="static/js/jquery-1.10.2.js"></script>
		<script src="static/js/login.js"></script>
	</head>
	<body>
		<div id="header"> Welcome! Please sign in..</div>
		<div class="bubble">
			<input type="text" id="user" placeholder=" username.." required>
			<input type="password" id="pass" placeholder=" password.." required>
			<button id="sign-in"> Sign in</button>
		</div>
		<div id="msg"></div>
	</body>
</html>
