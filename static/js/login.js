$(document).ready(function() {

	$('#sign-in').click(function() {
		$('#msg').removeClass('msg');
		$('#msg').empty();

		var obj = new Object();
		obj.username = $('#user').val();
		obj.password = $('#pass').val();
		var json = JSON.stringify(obj);

		$.ajax({
			type: "POST",
			url: "/rest/auth",
			data: json,
			statusCode: {
				200: function() {
					window.location = "/admin";
				},
				401: function() {
					$('#msg').addClass('msg');
					$('#msg').append('<strong>Wrong password or username!</strong> Please try again ');
				},
				500: function() {
					$('#msg').addClass('msg');
					$('#msg').append('<strong>Ops..</strong> Something goes wrong ');
				}
			}
		});
	});
});