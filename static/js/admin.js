var originalName = '';

$(document).ready(function() {
	$('ul > li').click(function() { 
		$('ul > li').removeClass('active'); 
		$(this).addClass('active'); 

		originalName = '';
		var trimmedName = $.trim($(this).text());
		init(trimmedName);		
	});

	$('#clear').click(function() {
		$('input').val(' ');
		$('#content').val(' ');
	});

	$('#new').click(function() {
		var obj = new Object();
		obj.name = $('#name').val();
		obj.content = $('#content').val();
		var json = JSON.stringify(obj);

		$.ajax({
			type: "PUT",
			url: "/rest/article",
			data: json,
			statusCode: {
				200: function() {
					jSuccess("<strong> Successfuly saved !");
				},
				404: function() {
					jError("<strong>Error!</strong> Ops.. ");
				},
				500: function() {
					jError("<strong>Error!</strong> Ops.. ");
				}
			}
		});
		location.reload();
	});

	$('#delete').click(function() {
		var name = $('#name').val();
		$.ajax({
			type: "DELETE",
			url: "/rest/article/" + name,
			statusCode: {
				200: function() {
					jSuccess("<strong> Successfuly deleted !");
				},
				404: function() {
					jError("<strong>Error!</strong> Ops.. ");
				},
				500: function() {
					jError("<strong>Error!</strong> Ops.. ");
				}
			}
		});

		location.reload();
	});

	$('#save').click(function() {
		var obj = new Object();
		obj.name = $('#name').val();
		obj.content = $('#content').val();
		obj.originalName = originalName;
		var json = JSON.stringify(obj);

		$.ajax({
			type: "POST",
			url: "/rest/article",
			data: json,
			statusCode: {
				200: function() {
					jSuccess("<strong> Successfuly updated");
				},
				404: function() {
					jError("<strong>Error!</strong> Ops.. ");
				},
				500: function() {
					jError("<strong>Error!</strong> Ops.. ");
				}
			}
		});
	});
});		

function init(name) {
	$('input').empty();
	$('article').empty();

	$.get('/rest/article/' + name, function(data) {
		$.each(data, function() {
			originalName = this.name;
			$('input').val(this.name);
			$('textarea').val(this.content);
		});
	});
}
