$(function() {
	$('ul > li').click(function() { 
	 	$('ul > li').removeClass('active'); 
		$(this).addClass('active');

		$('#article').empty();
		var trimmedName = $.trim($(this).text());
		var template = $('#template').html();
		$.get('/rest/article/' + trimmedName, function(data) {
			$.each(data, function() {
				data = {
					article: {
						name: this.name,
						content: this.content
					}
				}
				$('#article').append(nano(template, data));
			});
			prettyPrint();
		});
	});
});		