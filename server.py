import bottle
from bottle import Bottle, run, template, static_file, hook, request, route, response, error,abort
import bottle_mysql
import MySQLdb
import json
from beaker.middleware import SessionMiddleware


app = bottle.app()

plugin = bottle_mysql.Plugin(dbuser='root', dbpass='root', dbname='blog')
app.install(plugin)

session_opts = {
	'session.timeout': 900,
	'session.cookie_expires': 900,
    'session.type': 'file',
    'session.data_dir': './session/',
    'session.auto': True,
}

app_session = SessionMiddleware(app, session_opts)

@app.hook('before_request')
def setup_request():
    request.session = request.environ['beaker.session']

@app.hook('after_request')
def enableCORSAfterRequestHook():
    response.headers['Access-Control-Allow-Origin'] = '*'

@app.route('/rest/article/articlesList')
def get_articles_list(db):
	response.content_type = 'application/json'
	db.execute('SELECT name from t_article order by id desc')
	rows = db.fetchall()
	return json.dumps(rows)

# Rest api

@app.route('/rest/article/<name>')
def get_article_by_name(db, name):
	response.content_type = 'application/json'
	db.execute('SELECT * from t_article where name=%s order by id desc', name)
	rows = db.fetchall()
	return json.dumps(rows)

@error(404)
def custom404(error):			
    return 'reqeust body is empty'

@app.route('/rest/article', method='PUT')
def add_article(db):
	# try:
		data = json.load(request.body)
		query = 'INSERT INTO t_article (name, content) VALUES (%s,%s)'
		db.execute(query, (data["name"], data["content"]))
		db.close()
	# except ValueError, e:
	# 	return abort(404, "error ")

@app.route('/rest/article', method='POST')
def add_article(db):
	try:
		data = json.load(request.body)
		query = 'UPDATE t_article set name = %s, content = %s where name = %s'
		db.execute(query, (data["name"], data["content"], data["originalName"]))
		db.close()
	except ValueError, e:
		return abort(404, "error ")

@app.route('/rest/article/<name>', method='DELETE')
def delete(db, name):
	query = 'DELETE FROM t_article WHERE name=%s'
	db.execute(query, name)
	db.close()

@app.route('/rest/auth', method='POST')
def auth(db):
	data = json.load(request.body)
	db.execute('SELECT password from t_user where username = %s', data['username'])
	user = db.fetchone()
	# dict(rows[0])['name']
	if user is not None and user['password'] == data['password']:
		request.session['logged_in'] = True
		response.status = 200
	else:
		request.session['logged_in'] = False
		response.status = 401



# STATIC files api

@app.route("/static/css/<file>")
def static_css(file):
	return static_file(file, root='static/css/')

# return js file by nam from static folder 
@app.route("/static/js/<file>")
def static_js(file):
	return static_file(file, root='static/js/')

@app.route("/static/font/<file>")
def static_img(file):
	return static_file(file, root='static/font/')


# VIEW api

@app.route('/')
def index(db):
	db.execute('SELECT name from t_article order by id desc')
	rows = db.fetchall()
	return template('index.tpl', postlist=rows)

@app.route('/admin')
def admin(db):
	if 'logged_in' in request.session and request.session['logged_in'] is True:
		db.execute('SELECT name from t_article order by id desc')
		rows = db.fetchall()
		return template('admin.tpl', postlist=rows)
	else:
		return template('login.tpl')

@app.route('/login')
def login():
	return template('login.tpl')


# for dev 
run(app=app_session, host="0.0.0.0", port=8000)

# for production 
# run(app=app_session, host="0.0.0.0", port=8080, server="cherrypy")
